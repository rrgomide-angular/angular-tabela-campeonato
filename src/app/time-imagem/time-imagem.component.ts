import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-time-imagem',
  templateUrl: './time-imagem.component.html',
  styleUrls: ['./time-imagem.component.css']
})
export class TimeImagemComponent implements OnInit {
  @Input()
  imagem: string;

  constructor() {}

  ngOnInit() {}
}
