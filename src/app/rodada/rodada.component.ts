import { Jogo } from './../interfaces/Jogo';
import { ClassificacaoService } from './../services/classificacao.service';
import { Component, OnInit } from '@angular/core';
import { TimeFutebol } from '../interfaces/TimeFutebol';

@Component({
  selector: 'app-rodada',
  templateUrl: './rodada.component.html',
  styleUrls: ['./rodada.component.css']
})
export class RodadaComponent implements OnInit {
  times: TimeFutebol[];
  rodada: Jogo[];

  constructor(public service: ClassificacaoService) {}

  ngOnInit() {
    this.times = this.service.times;
    this.rodada = [];

    for (let i = 0; i < this.times.length; i += 2) {
      this.rodada.push({
        times: [this.times[i], this.times[i + 1]],
        placar: [0, 0]
      });
    }

    this.validarPlacar();
  }

  validarPlacar() {
    for (const jogo of this.rodada) {
      this.service.alterarResultadoJogo(jogo);
    }
  }
}
