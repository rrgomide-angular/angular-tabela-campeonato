import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RodadaComponent } from './rodada/rodada.component';
import { ClassificacaoComponent } from './classificacao/classificacao.component';
import { TimeImagemComponent } from './time-imagem/time-imagem.component';

@NgModule({
  declarations: [
    AppComponent,
    RodadaComponent,
    ClassificacaoComponent,
    TimeImagemComponent
  ],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
