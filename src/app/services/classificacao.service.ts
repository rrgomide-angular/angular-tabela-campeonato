import { Jogo } from './../interfaces/Jogo';
import { Injectable } from '@angular/core';
import { TimeFutebol } from '../interfaces/TimeFutebol';
import { cloneDeep } from 'lodash';

const cruzeiroImagem = '../../assets/cruzeiro.svg';
const galoImagem = '../../assets/atletico-mg.svg';
const vascoImagem = '../../assets/vasco.svg';
const saoPauloImagem = '../../assets/sao-paulo.svg';

@Injectable({
  providedIn: 'root'
})
export class ClassificacaoService {
  private _times: TimeFutebol[] = [];
  private _timesOriginal: TimeFutebol[] = [];

  constructor() {
    console.log('Construtor de service');

    this._times.push(
      {
        id: 1,
        nome: 'Cruzeiro',
        pontos: 23,
        sigla: 'CRU',
        imagem: cruzeiroImagem
      },
      {
        id: 2,
        nome: 'Atlético Mineiro',
        pontos: 22,
        sigla: 'ATL',
        imagem: galoImagem
      },
      {
        id: 3,
        nome: 'Vasco',
        pontos: 21,
        sigla: 'VAS',
        imagem: vascoImagem
      },
      {
        id: 4,
        nome: 'São Paulo',
        pontos: 20,
        sigla: 'SPO',
        imagem: saoPauloImagem
      }
    );

    /**
     * Clonando a classificação original
     */
    this._timesOriginal = cloneDeep(this._times);
  }

  alterarResultadoJogo(jogo: Jogo) {
    /**
     * Guardando os gols de cada time
     */
    const golsTime1 = jogo.placar[0];
    const golsTime2 = jogo.placar[1];

    /**
     * Obtendo cópias dos objetos
     * originais dos times
     */
    const time1Atual = cloneDeep(
      this._timesOriginal.find(
        (time: TimeFutebol) => jogo.times[0].id === time.id
      )
    );

    const time2Atual = cloneDeep(
      this._timesOriginal.find(
        (time: TimeFutebol) => jogo.times[1].id === time.id
      )
    );

    /**
     * Definindo pontuanção de
     * cada time
     */
    if (golsTime1 === golsTime2) {
      time1Atual.pontos++;
      time2Atual.pontos++;
    } else {
      if (golsTime1 > golsTime2) {
        time1Atual.pontos += 3;
      } else {
        time2Atual.pontos += 3;
      }
    }

    /**
     * Atualizando placar dos times
     * no vetor "oficial" de forma
     * imutável
     */
    const indexTime1 = this._times.findIndex(
      (time: TimeFutebol) => time.id === time1Atual.id
    );

    const indexTime2 = this._times.findIndex(
      (time: TimeFutebol) => time.id === time2Atual.id
    );

    this._times[indexTime1] = time1Atual;
    this._times[indexTime2] = time2Atual;
  }

  get times(): TimeFutebol[] {
    const timesOrdenados: TimeFutebol[] = [].concat(this._times);
    timesOrdenados.sort(
      (a: TimeFutebol, b: TimeFutebol) => b.pontos - a.pontos
    );
    return timesOrdenados;
  }
}
