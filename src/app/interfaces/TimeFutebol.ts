export interface TimeFutebol {
  id: number;
  nome: string;
  pontos: number;
  imagem: string;
  sigla: string;
}
