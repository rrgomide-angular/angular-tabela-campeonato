import { TimeFutebol } from './TimeFutebol';

export interface Jogo {
  times: TimeFutebol[];
  placar: number[];
}
