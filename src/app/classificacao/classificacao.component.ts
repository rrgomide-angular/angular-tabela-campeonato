import { ClassificacaoService } from './../services/classificacao.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-classificacao',
  templateUrl: './classificacao.component.html',
  styleUrls: ['./classificacao.component.css']
})
export class ClassificacaoComponent {
  constructor(public service: ClassificacaoService) {}
}
